byte pins[] = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

byte digit1 = 13;
byte digit2 = 12;
byte digit3 = 11;
byte digit4 = 10;

byte top = 2;
byte topRight = 3;
byte bottomRight = 4;
byte bottom = 5;
byte bottomLeft = 6;
byte topLeft = 7;
byte middle = 8;
byte dot = 9;

byte digits[] = {
  B11111100,
  B01100000,
  B11011010,
  B11110010,
  B01100110,
  B10110110,
  B10111110,
  B11100000,
  B11111110,
  B11100110
};

byte unit = 0;
byte ten = 0;
byte hundred = 0;
byte thousand = 0;

void setup()
{
  for (byte pin = 2; pin < 14; pin++)
  {
    pinMode(pin, OUTPUT);
  }
}

void hide_all_digits()
{
  digitalWrite(digit1, HIGH);
  digitalWrite(digit2, HIGH);
  digitalWrite(digit3, HIGH);
  digitalWrite(digit4, HIGH);
}

void show_all_digits()
{
  digitalWrite(digit1, LOW);
  digitalWrite(digit2, LOW);
  digitalWrite(digit3, LOW);
  digitalWrite(digit4, LOW);
}

void show_thousand_digit()
{
  digitalWrite(digit1, LOW);
  digitalWrite(digit2, HIGH);
  digitalWrite(digit3, HIGH);
  digitalWrite(digit4, HIGH);
}

void show_hundred_digit()
{
  digitalWrite(digit1, HIGH);
  digitalWrite(digit2, LOW);
  digitalWrite(digit3, HIGH);
  digitalWrite(digit4, HIGH);
}

void show_ten_digit()
{
  digitalWrite(digit1, HIGH);
  digitalWrite(digit2, HIGH);
  digitalWrite(digit3, LOW);
  digitalWrite(digit4, HIGH);
}

void show_unit_digit()
{
  digitalWrite(digit1, HIGH);
  digitalWrite(digit2, HIGH);
  digitalWrite(digit3, HIGH);
  digitalWrite(digit4, LOW);
}

void show_digit(byte digit)
{
  byte bin = digits[digit];
  for (byte pin = 2; pin < 9; pin++)
  {
    auto value = (bitRead(bin, 7 - (pin - 2))) ? HIGH : LOW;
    digitalWrite(pin, value);
  }
}

void clear_digit()
{
  for (byte pin = 2; pin < 9; pin++)
  {
    digitalWrite(pin, LOW);
  }
}

void show_number()
{
  show_digit(unit);
  show_unit_digit();
  delay(5);

  show_digit(ten);
  show_ten_digit();
  delay(5);

  show_digit(hundred);
  show_hundred_digit();
  delay(5);

  show_digit(thousand);
  show_thousand_digit();
  delay(5);
}

void loop()
{
  for (thousand = 0; thousand < 10; thousand++)
  {
    for (hundred = 0; hundred < 10; hundred++)
    {   
      for (ten = 0; ten < 10; ten++)
      { 
        for (unit = 0; unit < 10; unit++)
        {
          unsigned long start = millis();
          while (millis() - start < 200) 
          {
            show_number();
          }
        }
      }
    }
  }
}
